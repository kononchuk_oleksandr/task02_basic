package com.axeane.firstapp;

public final class Main {

    private Main() {
    }

    public static void main(final String[] args) {
        OperationsWithNumbers operationsWithNum = new OperationsWithNumbers();

        operationsWithNum.printNumbers();
        operationsWithNum.printSumOfNumbers();
        operationsWithNum.printFibonacciNumbers();
    }
}
