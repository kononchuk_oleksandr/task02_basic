/**
 * Class for work with console.
 */

package com.axeane.firstapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConsoleHelper {
    private BufferedReader reader = new BufferedReader(
        new InputStreamReader(System.in, StandardCharsets.UTF_8));

    public final String getMessage() {
        String message = null;
        try {
            message = reader.readLine();
        } catch (IOException e) {
            e.getMessage();
        }
        return message;
    }

    /**
     * Method check the string from console and
     * @return an array of two numbers.
     */
    public final int[] getInterval() {
        System.out.println("Please print two numbers and split them by "
            + "semicolon (Example: 1;100):");
        String message = getMessage();
        Pattern pattern = Pattern.compile("^\\d+;\\d+$");
        Matcher matcher = pattern.matcher(message);
        while (true) {
            if (!matcher.matches()) {
                System.out.println("Message is not valid. Please try again.");
                message = getMessage();
            } else {
                break;
            }
        }
        return Arrays.stream(message.split(";"))
                .mapToInt(Integer::parseInt)
                .toArray();
    }
}
