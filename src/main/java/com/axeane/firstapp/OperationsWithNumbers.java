/**
* This class contains all operations with numbers required for homework
*/

package com.axeane.firstapp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class OperationsWithNumbers {
    private ConsoleHelper consoleHelper = new ConsoleHelper();
    private int[] intArray = consoleHelper.getInterval();
    private int firstNumber = intArray[0];
    private int lastNumber = intArray[1];

    /**
     * @return list of odd numbers
     */
    private List<Integer> oddNumbersList() {
        List<Integer> oddNumbers = new ArrayList<>();
        for (int i = firstNumber; i <= lastNumber; i++) {
            if (i % 2 != 0) {
                oddNumbers.add(i);
            }
        }
        return oddNumbers;
    }

    /**
     * @return list of even numbers
     */
    private List<Integer> evenNumbersList() {
        List<Integer> evenNumbers = new ArrayList<>();
        for (int i = firstNumber; i <= lastNumber; i++) {
            if (i % 2 == 0) {
                evenNumbers.add(i);
            }
        }
        return evenNumbers;
    }

    /**
     * Method prints odd numbers from start to the end
     * of interval and even from end to start.
     */
    public final void printNumbers() {
        System.out.println("Odd numbers: "
            + String.join(", ", oddNumbersList().toString()));
        System.out.println("Even numbers: "
            + String.join(", ", evenNumbersList().stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList())
                .toString()));
    }

    /**
     * Method prints the sum of odd and even numbers.
     */
    public final void printSumOfNumbers() {
        int sumOfOddNumbers = 0;
        int sumOfEvenNumbers = 0;

        for (int i = 0; i < oddNumbersList().size(); i++) {
            sumOfOddNumbers += oddNumbersList().get(i);
        }
        for (int i = 0; i < evenNumbersList().size(); i++) {
            sumOfEvenNumbers += evenNumbersList().get(i);
        }

        System.out.println("Sum of odd numbers is: " + sumOfOddNumbers);
        System.out.println("Sum of even numbers is: " + sumOfEvenNumbers);
    }

    /**
     * Method build Fibonacci numbers where F1 will be the biggest odd number
     * and F2 – the biggest even number, user enter the size of set (N).
     * Also prints percentage of odd and even Fibonacci numbers.
     */
    public final void printFibonacciNumbers() {
        int fibonacciOddNum = 0;
        int fibonacciEvenNum = 0;
        System.out.println("Please enter the number for size of Fibonacci set: ");
        final int N = Integer.parseInt(consoleHelper.getMessage());
        List<Integer> fibonacciList = new ArrayList<>(Arrays.asList(firstNumber,
            lastNumber));
        int number;

        for (int i = 2; i < N; ++i) {
            number = fibonacciList.get(i - 1) + fibonacciList.get(i - 2);
            fibonacciList.add(number);
        }
        System.out.println("Result is: " + String.join(", ",
            fibonacciList.toString()));

        for (int i = 0; i < fibonacciList.size(); i++) {
            if (fibonacciList.get(i) % 2 != 0) {
                fibonacciOddNum++;
            } else {
                fibonacciEvenNum++;
            }
        }
        System.out.println("Percent of Fibonacci's odd numbers is: "
            + (float)(fibonacciOddNum * 100) / fibonacciList.size());
        System.out.println("Percent of Fibonacci's even numbers is: "
            + (float)(fibonacciEvenNum * 100) / fibonacciList.size());
    }
}
